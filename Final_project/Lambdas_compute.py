from Final_project.Past_stuff.Physical_models import density, gravity
import math
import numpy as np


def lambdas(aoa, k, t, h, gamma, v):
    CD_slope, CL_0, CD_0 = 1.085, 0.1, 0.8
    S_ref = 11
    mass = 461
    Rp = 3396e3
    mu = 42828.3736206991
    g0 = 3.71
    H = 6.308278108290950e+03
    # evaluate lambda_switch
    lambda_switch = np.divide(k * 2.0 * mass * v,
                              S_ref * CD_slope * math.pi)
    lambdav = [0] * len(t)
    lambdag = [0] * len(t)
    lambdah = [0] * len(t)
    lambdav[-1] = v[-1]
    lambdag[-1] = 0.0
    lambdah[-1] = mu / (Rp + h[-1]) ** 2
    rho = density(h)
    # g = np.divide(mu,np.square(Rp+h))
    g = gravity(h)  # np.divide(-mu,np.square(Rp+h))
    for ii in range(len(t) - 1, 0, -1):  # reversed(t):
        # print(ii)
        lambdav_dot = -3 * k * rho[ii - 1] * v[ii - 1] ** 2 * aoa[ii - 1] / math.pi + lambdav[ii] * (
                rho[ii - 1] * S_ref * (CD_0 + aoa[ii - 1] * CD_slope) * v[ii - 1]) / mass - \
                      lambdag[ii] * ((rho[ii - 1] * S_ref * CL_0) / (2 * mass) + g[ii - 1] / v[ii - 1] ** 2 + 1 / (
                Rp + h[ii - 1])) - lambdah[ii] * gamma[ii - 1]
        lambdag_dot = lambdav[ii] * g[ii - 1] - lambdah[ii] * v[ii - 1]
        lambdah_dot = k * rho[ii - 1] * v[ii - 1] ** 3 * aoa[ii - 1] / (math.pi * H) - lambdav[ii] * (
                (rho[ii - 1] * S_ref * (CD_0 + aoa[ii - 1] * CD_slope) * v[ii - 1] ** 2) / (2 * mass * H) + 2 * g[
            ii - 1] * gamma[ii - 1] / (Rp + h[ii - 1])) \
                      + lambdag[ii] * (rho[ii - 1] * S_ref * CL_0 * v[ii - 1] / (2 * mass * H) - 2 * g[ii - 1] / (
                (Rp + h[ii - 1]) * v[ii - 1]) + v[ii - 1] / (Rp + h[ii - 1]) ** 2)
        lambdav[ii - 1] = lambdav[ii] - lambdav_dot * (t[ii] - t[ii - 1])
        # print(lambdav[ii-1])
        lambdag[ii - 1] = lambdag[ii] - lambdag_dot * (t[ii] - t[ii - 1])
        lambdah[ii - 1] = lambdah[ii] - lambdah_dot * (t[ii] - t[ii - 1])

    in_cond_lambda = [lambdav[1], lambdag[1], lambdah[1]]
    # import matplotlib.pyplot as plt
    #
    # plt.plot(t,lambdav)
    # plt.plot(t,lambda_switch)
    # plt.show()
    return lambda_switch, lambdav, in_cond_lambda



