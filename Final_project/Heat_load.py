import math
import numpy as np
from planet import Planet, mars_props
from vehicle import Vehicle, heat_rate_props
from Control_logic import Trajectory
import matplotlib.pyplot as plt
from AoA_profile import get_aoa_profile
from utils import get_qdot_trajectory


heat_rate = Vehicle(properties=heat_rate_props)
mars = Planet(properties=mars_props)

solutions = Trajectory(vehicle=heat_rate, planet=mars)
v, h, gamma, t, aoa_profile = solutions.closed_form()


t_cf = np.linspace(0, 461, num=2000)
test = [90] * len(t_cf)

lambda_switch, lambdav, in_cond_lambda = solutions.lambdas(np.deg2rad(test), 0.04058, t, h, gamma, v)

index_array = (lambdav < lambda_switch)
temp = t * index_array
temp = np.trim_zeros(temp)
t_switch = [temp[0], temp[-1]]

fig1 = plt.figure(1)
plt.title('Heat load switching times')
plt.ylabel('Angle of attack, deg')
plt.xlabel('Time')
plt.plot([0, t_switch[0]], [90, 90])
plt.plot([t_switch[0], t_switch[1]], [0, 0])
plt.plot([t_switch[1], t[-1]], [90, 90])
plt.plot([t_switch[0], t_switch[0]], [90, 0])
plt.plot([t_switch[1], t_switch[1]], [0, 90])
plt.show()
fig1.savefig('AoA_profile_heat_load')

fig2 = plt.figure(2)
plt.plot(t, lambdav)
plt.plot(t, lambda_switch, color='blue')
plt.show()

fig2.savefig('Lambdav_lambdaSwitch')

aoa = []
for val in index_array:
    if val == True:
        aoa_temp = 0
        aoa.append(aoa_temp)
    elif val == False:
        aoa_temp = 90
        aoa.append(aoa_temp)

plt.figure()
# plt.plot(t, aoa)
# plt.show()

v_sol, h_sol, gamma_sol, t_sol, aoa_profile_sol = solutions.closed_form(aoa_profile=aoa)


q_dot = get_qdot_trajectory(v, aoa, mars.rho(h))
Q = np.cumsum(q_dot) * t[-1] / len(t)
print(Q[-1])

fig0, ax0 = plt.subplots(3, 1, sharex=True)
fig0.suptitle('Heat load control Solution')
ax0[0].plot(t, aoa_profile_sol)
ax0[1].plot(t, q_dot)
ax0[1].plot([0, t[-1]], [0.16, 0.16], linewidth=1, linestyle='--')
ax0[2].plot(t, Q)

ax0[0].set_ylabel('AoA, deg')
ax0[1].set_ylabel('Heat rate, W/cm^2')
ax0[2].set_ylabel('Heat load, J/cm^2')
ax0[2].set_xlabel('Time, s')

ax0[2].set_ylim(-5, 50)

plt.show()
fig0.savefig('Solution_Heat_load')