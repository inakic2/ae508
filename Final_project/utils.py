import numpy as np
import math
from scipy import optimize


def get_angle(rho, v):
    R = 188.92
    T_p = 150
    S = v / (2 * R * T_p) ** 0.5
    L = (0.2 * rho * R * T_p) * ((R * T_p / (2.0 * math.pi)) ** 0.5) * 1e-4
    heat_limit = 0.16

    #  Schaaf and Chambre heat rate equation setup for Newton method
    #  This computes the maximum allowable angle for a given heat rate limit

    def f(x):
        fx = L * ((S ** 2 + 0.5) * (math.exp(-(S * math.sin(x)) ** 2) + (math.pi ** 0.5) * (S * math.sin(x)) *
                                    (1 + math.erf(S * math.sin(x)))) - 0.5 * math.exp(
            -(S * math.sin(x)) ** 2)) - heat_limit

        return fx

    Angle_of_attack = optimize.newton(f, np.deg2rad(20), fprime=lambda x: L * S * math.cos(x) * (
            (math.pi ** 0.5) * (S ** 2 + 0.5) * (1 + math.erf(S * math.sin(x))) + S * math.sin(x) * math.exp(
        -(S * math.sin(x)) ** 2)), tol=1e-5, maxiter=1000)

    # print(np.rad2deg(Angle_of_attack))
    return np.rad2deg(Angle_of_attack)

def get_qdot_trajectory(v, AoA, Rho):
    R = 188.92
    T_p = 150
    u_vals = np.deg2rad(AoA)
    q_dot = []
    for rho, x, v in zip(Rho, u_vals, v):
        S = v / (2 * R * T_p) ** 0.5
        L = (0.2 * rho * R * T_p) * ((R * T_p / (2.0 * math.pi)) ** 0.5) * 1e-4
        q_dot_vals = L * ((S ** 2 + 0.5) * (math.exp(-(S * math.sin(x)) ** 2) + (math.pi ** 0.5) * (S * math.sin(x)) *
                                            (1 + math.erf(S * math.sin(x)))) - 0.5 * math.exp(-(S * math.sin(x)) ** 2))
        q_dot.append(q_dot_vals)

    return q_dot


def get_qdot(v_0, AoA, rho):
    R = 188.92
    T_p = 150

    x = np.deg2rad(AoA)
    S = v_0 / (2 * R * T_p) ** 0.5
    L = (0.2 * rho * R * T_p) * ((R * T_p / (2.0 * math.pi)) ** 0.5) * 1e-4
    q_dot = L * ((S ** 2 + 0.5) * (math.exp(-(S * math.sin(x)) ** 2) + (math.pi ** 0.5) * (S * math.sin(x)) *
                                   (1 + math.erf(S * math.sin(x)))) - 0.5 * math.exp(-(S * math.sin(x)) ** 2))
    return q_dot
