import math
import numpy as np
from planet import Planet, mars_props
from vehicle import Vehicle, heat_rate_props
import matplotlib.pyplot as plt
from AoA_profile import get_aoa_profile
from utils import get_qdot_trajectory


class Trajectory:
    def __init__(self, vehicle: Vehicle, planet: Planet):
        self.t_span = None
        self.vehicle = vehicle
        self.planet = planet

    def closed_form(self, aoa_profile=[]):

        t_f = 465
        t_cf = np.linspace(0, t_f, num=2000)
        t_p = t_f / 2

        if not aoa_profile:
            aoa_profile = [self.vehicle.aoa] * len(t_cf)
        else:
            if len(aoa_profile) > len(t_cf):
                aoa_profile = aoa_profile[0:len(t_cf)]
            elif len(aoa_profile) < len(t_cf):
                last_aoa = aoa_profile[-1]
                aoa_profile = aoa_profile + [last_aoa] * (len(t_cf) - len(aoa_profile))

        CD_t = np.add(self.vehicle.Cd_0, np.divide(np.multiply(aoa_profile, self.vehicle.Cd_slope), math.pi / 2))
        CL_t = self.vehicle.Cl_0
        cost_3 = self.vehicle.v0 * self.vehicle.gamma0
        h_cf = self.vehicle.h0 + cost_3 * (t_cf - (np.square(t_cf) / (2 * t_p)))
        rho = self.planet.rho(h_cf)

        cost_1 = np.divide(rho * CD_t * self.vehicle.S_ref_total, 2 * self.vehicle.m)  # CORRECT
        cost_2 = np.divide(rho * CL_t * self.vehicle.S_ref_total, 2 * self.vehicle.m)  # CORRECT

        a0 = 0.0016  # 0.0034
        c0 = 5e-06  # 2.9648e-06
        mean_a = 3.38  # 3.5839
        mean_c = 2.6  # 2.5413
        mean_b = -8.25  # -14.5169
        mean_d = -0.001  # 2.5413

        f1 = -0.005 * self.vehicle.v0 + 27.87  # CORRECT
        f2 = (a0 * (mean_a ** (2 * abs(math.degrees(self.vehicle.gamma0) + 3)) * math.exp(
            mean_b * (self.vehicle.v0 / 1000 - 3.7))) + c0 * (
                      mean_c ** (2 * abs(math.degrees(self.vehicle.gamma0) + 3)) * math.exp(
                  mean_d * (self.vehicle.v0 / 1000 - 3.7)))) * (t_cf) / (
                     2 * t_p)

        f2_solar_panels = f2 * self.vehicle.aoa * self.vehicle.S_ref_SA / self.vehicle.S_ref_total
        f2_spacecraft = f2 * math.pi / 2 * self.vehicle.S_ref_SC / self.vehicle.S_ref_total

        epsilon = f1 + f2_solar_panels + f2_spacecraft

        k1 = (cost_2 + np.divide(1, (self.planet.Rp + h_cf)))  # CORRECT
        k2 = np.multiply(cost_1 * cost_3, (1 - t_cf / t_p))  # CORRECT
        k3 = -self.planet.g_ref - epsilon  # CORRECT
        cost = self.vehicle.v0 - (k2[0] / k1[0] - ((k2[0] / k1[0]) ** 2 - 4 * (k3[0] / k1[0])) ** 0.5) / 2
        # Calculating velocity
        v_cf = (np.divide(k2, k1) - np.sqrt(
            np.square(np.divide(k2, k1)) - 4 * np.divide(k3, k1))) / 2 + cost

        #  Calculating gamma
        gamma_cf = cost_3 * np.divide((1 - t_cf / t_p), v_cf)
        # t_cf = np.array([item + t0 for item in t_cf])
        return v_cf, h_cf, gamma_cf, t_cf, aoa_profile

    def lambdas(self, aoa, k, t, h, gamma, v):
        CD_slope, CL_0, CD_0 = self.vehicle.Cd_slope, self.vehicle.Cl_0, self.vehicle.Cd_0
        S_ref = self.vehicle.S_ref_total
        mass = self.vehicle.m
        Rp = self.planet.Rp
        mu = self.planet.mu
        g0 = self.planet.g_ref
        H = self.planet.H
        # evaluate lambda_switch
        lambda_switch = np.divide(k * 2.0 * mass * v,
                                  S_ref * CD_slope * math.pi)
        lambdav = [0] * len(t)
        lambdag = [0] * len(t)
        lambdah = [0] * len(t)
        lambdav[-1] = v[-1]
        lambdag[-1] = 0.0
        lambdah[-1] = mu / (Rp + h[-1]) ** 2
        rho = self.planet.rho(h)
        # g = np.divide(mu,np.square(Rp+h))
        g = self.planet.grav(h)  # np.divide(-mu,np.square(Rp+h))
        for ii in range(len(t) - 1, 0, -1):  # reversed(t):
            # print(ii)
            lambdav_dot = -3 * k * rho[ii - 1] * v[ii - 1] ** 2 * aoa[ii - 1] / math.pi + lambdav[ii] * (
                    rho[ii - 1] * S_ref * (CD_0 + aoa[ii - 1] * CD_slope) * v[ii - 1]) / mass - \
                          lambdag[ii] * ((rho[ii - 1] * S_ref * CL_0) / (2 * mass) + g[ii - 1] / v[ii - 1] ** 2 + 1 / (
                    Rp + h[ii - 1])) - lambdah[ii] * gamma[ii - 1]
            lambdag_dot = lambdav[ii] * g[ii - 1] - lambdah[ii] * v[ii - 1]
            lambdah_dot = k * rho[ii - 1] * v[ii - 1] ** 3 * aoa[ii - 1] / (math.pi * H) - lambdav[ii] * (
                    (rho[ii - 1] * S_ref * (CD_0 + aoa[ii - 1] * CD_slope) * v[ii - 1] ** 2) / (2 * mass * H) + 2 * g[
                ii - 1] * gamma[ii - 1] / (Rp + h[ii - 1])) \
                          + lambdag[ii] * (rho[ii - 1] * S_ref * CL_0 * v[ii - 1] / (2 * mass * H) - 2 * g[ii - 1] / (
                    (Rp + h[ii - 1]) * v[ii - 1]) + v[ii - 1] / (Rp + h[ii - 1]) ** 2)
            lambdav[ii - 1] = lambdav[ii] - lambdav_dot * (t[ii] - t[ii - 1])
            # print(lambdav[ii-1])
            lambdag[ii - 1] = lambdag[ii] - lambdag_dot * (t[ii] - t[ii - 1])
            lambdah[ii - 1] = lambdah[ii] - lambdah_dot * (t[ii] - t[ii - 1])

        in_cond_lambda = [lambdav[1], lambdag[1], lambdah[1]]
        # import matplotlib.pyplot as plt
        #
        # plt.plot(t,lambdav)
        # plt.plot(t,lambda_switch)
        # plt.show()
        return lambda_switch, lambdav, in_cond_lambda

#
# times = np.concatenate(([0, t_switch[0]], [t_switch[0], t_switch[0]], [t_switch[0], t_switch[1]],
#                         [t_switch[1], t_switch[1]], [t_switch[1], t[-1]]))
# angles = np.concatenate(([90, 90], [90, 0], [0, 0], [0, 90], [90, 90]))
#
# plt.figure()
# plt.plot(times, angles)
# plt.show()
#



# solutions2 = Trajectory(vehicle=heat_rate, planet=mars)
# v2, h2, gamma2, t2, aoa_profile3 = solutions.closed_form(aoa_profile=aoa_profile2)
#
# plt.figure(1)
# plt.plot(t2, v2, color='blue')
# plt.plot(t, v)
# plt.show()
#
# plt.figure(2)
# plt.plot(t2, h2)
# plt.plot(t, h, color='blue', linewidth=2, linestyle='--')
# plt.show()


# aoa_profile = np.array([0, 0, 0, 0, 0, 0])
# solution2 = Trajectory(vehicle=heat_rate, planet=mars)
# v2, h2, gamma2, t2 = solutions.closed_form(np.deg2rad(aoa_profile))

#     def sol_closed_form(self):
#
# def Closed_form_calculation(v0, h0, gamma0, aoa, aoa_profile=[]):
#     CD_bar = 1.085
#     CD0 = 0.8
#     # v0 = 4.62e3
#     Area_tot = 11
#     Area_SA = 3.7
#     Area_SC = 7.3
#     mass = 461
#     # gamma0 = np.deg2rad(-7.6)
#     # h0 = 160e3
#     Rp = 3396e3
#     g_ref = 3.71
#     t_f = 461
#     t_cf = np.linspace(0, t_f, num=2000)
#     t_p = t_f / 2
#     # aoa = np.deg2rad(0)
#
#     return v_cf, h_cf, gamma_cf, t_cf
