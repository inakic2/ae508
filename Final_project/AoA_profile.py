from Final_project.Past_stuff.Physical_models import density
import numpy as np
from utils import get_angle, get_qdot


def get_aoa_profile(v, h, gamma):
    #  v0, h0, gamma0, aoa, aoa_profile=[]
    Rho = density(h)
    heat_limit = 0.16
    u_vals = []
    for rho_t, v in zip(Rho, v):

        qdot_max = get_qdot(v, 90, rho_t)
        qdot_min = get_qdot(v, 0, rho_t)

        if qdot_max < heat_limit:
            u = 90
            u_vals.append(u)
        elif qdot_min > heat_limit:
            u = 0.00001
            u_vals.append(u)
        elif qdot_max >= heat_limit >= qdot_min:
            u = get_angle(rho_t, v)
            u_vals.append(u)

    u_profile = np.array(u_vals)

    return u_profile
#
# u_profile, t = get_aoa_profile()
#
# plt.plot(t, u_profile)
# plt.grid()
# plt.show()

# u_profile, t, h, t_f, Rho = get_aoa_profile()
#
# plt.figure(6)
# plt.plot(t, h)
# plt.grid()
# plt.show()
#
# q_vals = get_qdot_trajectory(4.62e3, u_profile, Rho)
#
# fig0, ax0 = plt.subplots(2, 1, sharex=True)
# fig0.suptitle('Angle of attack, heat rate')
# ax0[0].plot(t, u_profile)
# ax0[1].plot(t, q_vals)
# ax0[1].plot([0, t_f], [0.1, 0.1], linewidth=1, linestyle='--')
#
# ax0[0].set_ylabel('AoA, deg')
# ax0[1].set_ylabel('Heat rate, W/cm^2')
# plt.grid()
#
# plt.show()
