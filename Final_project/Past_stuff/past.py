from AoA_profile import get_aoa_profile
import math
import numpy as np
from Physical_models import density, gravity
import matplotlib.pyplot as plt


aoa_profile = np.deg2rad(get_aoa_profile())
# S = (v0 / (2 * RT) ** 0.5)
CD_bar = 2.4
CD0 = 0.8
v0 = 4.2e3
Area_tot = 11
Area_SA = 3.7
Area_SC = 7.3
mass = 461
gamma0 = np.deg2rad(-6.1)
h0 = 160e3
Rp = 3396e3
g_ref = 3.71

t_f = 750
t = np.linspace(0, t_f, num=1000)

t_p = t_f / 2

CD_t = np.add(CD0, np.divide(np.multiply(aoa_profile, CD_bar), math.pi / 2))
aoa = aoa_profile

v_cf_traj = []
h_cf_traj = []
gamma_cf_traj = []

for t_cf, CD_t, aoa in zip(t, CD_t, aoa):

    CL_t = 0.1
    cost_3 = v0 * gamma0
    h_cf = h0 + cost_3 * (t_cf - (np.square(t_cf) / (2 * t_p)))
    rho = density(h_cf)

    cost_1 = np.divide(rho * CD_t * Area_tot, 2 * mass)  # CORRECT
    cost_2 = np.divide(rho * CL_t * Area_tot, 2 * mass)  # CORRECT

    a0 = 0.0016  # 0.0034
    c0 = 5e-06  # 2.9648e-06
    mean_a = 3.38  # 3.5839
    mean_c = 2.6  # 2.5413
    mean_b = -8.25  # -14.5169
    mean_d = -0.001  # 2.5413

    f1 = -0.005 * v0 + 27.87  # CORRECT
    f2 = (a0 * (mean_a ** (2 * abs(math.degrees(gamma0) + 3)) * math.exp(mean_b * (v0 / 1000 - 3.7))) + c0 * (
            mean_c ** (2 * abs(math.degrees(gamma0) + 3)) * math.exp(mean_d * (v0 / 1000 - 3.7)))) * (t_cf) / (2 * t_p)

    f2_solar_panels = f2 * aoa * Area_SA / Area_tot
    f2_spacecraft = f2 * math.pi / 2 * Area_SC / Area_tot

    epsilon = f1 + f2_solar_panels + f2_spacecraft

    k1 = (cost_2 + np.divide(1, (Rp + h_cf)))  # CORRECT
    k2 = np.multiply(cost_1 * cost_3, (1 - t_cf / t_p))  # CORRECT
    k3 = -g_ref - epsilon  # CORRECT
    # cost = v0 - (k2[0] / k1[0] - ((k2[0] / k1[0]) ** 2 - 4 * (k3[0] / k1[0])) ** 0.5) / 2
    cost = 10333.71689797462

    #  Calculating velocity
    v_cf = (np.divide(k2, k1) - np.sqrt(
        np.square(np.divide(k2, k1)) - 4 * np.divide(k3, k1))) / 2 + cost

    #  Calculating gamma
    gamma_cf = cost_3 * np.divide((1 - t_cf / t_p), v_cf)
    # t_cf = np.array([item + t0 for item in t_cf])

    v_cf_traj.append(v_cf)
    h_cf_traj.append(h_cf)
    gamma_cf_traj.append(gamma_cf)


plt.plot(t, v_cf_traj)
plt.show()




