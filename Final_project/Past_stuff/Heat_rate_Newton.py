import numpy as np
import math
from scipy import optimize
from Simulation import *


def get_angle(rho, v_0):
    alpha = 0.2
    R = 188.92
    T_p = 150
    S = v_0 / (2 * R * T_p) ** 0.5
    L = (0.2 * rho * R * T_p) * ((R * T_p / (2.0 * math.pi)) ** 0.5) * 1e-4
    heat_limit = 0.1
    gamma = 1.33

    #  Schaaf and Chambre heat rate equation setup for Newton method
    #  This computes the maximum allowable angle for a given heat rate limit

    def f(x):
        fx = L * ((S ** 2 + 0.5) * (math.exp(-(S * math.sin(x)) ** 2) + (math.pi ** 0.5) * (S * math.sin(x)) *
                                    (1 + math.erf(S * math.sin(x)))) - 0.5 * math.exp(
            -(S * math.sin(x)) ** 2)) - heat_limit

        return fx

    Angle_of_attack = optimize.newton(f, np.deg2rad(20), fprime=lambda x: L * S * math.cos(x) * (
            (math.pi ** 0.5) * (S ** 2 + 0.5) * (1 + math.erf(S * math.sin(x))) + S * math.sin(x) * math.exp(
        -(S * math.sin(x)) ** 2)), tol=1e-5, maxiter=1000)

    # print(np.rad2deg(Angle_of_attack))
    return np.rad2deg(Angle_of_attack)


