#  Defining the models for atmosphere and gravity
import numpy as np


def density(h):
    Rho_0 = 8.748923102971180e-07
    H = 6.308278108290950e+03
    Rho = Rho_0 * np.exp((90e3 - h) / H)

    return Rho


def gravity(h):
    g0 = 3.71
    Rp = 3.3962e6
    g = g0 * (Rp / (Rp + h)) ** 2

    return g

