#  This file will test the dynamics of the system with max and min Cd values to make sure it does not enter #
import numpy as np
from scipy.integrate import solve_ivp, odeint
import matplotlib.pyplot as plt
import math


def density(h):
    Rho_0 = 8.748923102971180e-07
    H = 6.308278108290950e+03
    Rho = Rho_0 * np.exp((90e3 - h) / H)

    return Rho


def gravity(h):
    g0 = 3.71
    Rp = 3.3962e6
    g = g0 * (Rp / (Rp + h)) ** 2

    return g


def DOF_Dynamics(t, x, Rp, Cd_0, Cd_average, Cl, S_ref, m, u):
    h_ref = 90e3
    H = 6.308278108290950e+03
    #  Two-degree-of-freedom equations of motion, states
    v_dot = - ((((density(x[2]) * S_ref * (Cd_0 + u * Cd_average)) / (2 * m)) * x[0] ** 2) + (
            gravity(x[2]) * x[1]))
    gamma_dot = ((density(x[2]) * S_ref * Cl * x[0]) / 2 / m) - (gravity(x[2]) / x[0]) + (x[0] / (Rp + x[2]))
    h_dot = x[0] * x[1]

    # #  Costate equations
    #
    # lambda_v_dot = -3 * K * density(x[2]) * x[0] ** 2 * (u / np.pi) + \
    #                lambda_v * ((density(x[2]) * S_ref * (Cd_0 + u * Cd_average) * x[0]) / m) - \
    #                lambda_gamma * (((density(x[2]) * S_ref * Cl) / (2 * m)) + (gravity(x[2]) / x[0] ** 2) + (
    #             1 / (Rp + x[2]))) - \
    #                lambda_h * x[1]
    #
    # lambda_gamma_dot = lambda_v * gravity(x[2]) - lambda_h * x[0]
    #
    # lambda_h_dot = (K * density(x[2]) * x[0] ** 3 * u) / np.pi * H - \
    #                lambda_v ** ((density(x[2]) * S_ref * (Cd_0 + u * Cd_average) * x[0] ** 2) / (2 * m * H) + ((2 * gravity(x[2]) * x[1]) / (Rp + h_ref + x[2]))) + \
    #                lambda_gamma * (((density(x[2]) * S_ref * Cl * x[0]) / 2 * m * H) - (2 * gravity(x[2])) / ((Rp + h_ref + x[2]) * x[0]) + (x[0] / (Rp + x[2]) ** 2))

    return np.hstack([v_dot, gamma_dot, h_dot])


def limit(t, y, Rp, Cd_0, Cd_average, Cl, S_ref, m, u):
    return np.array(y[2] - 160000)


limit.terminal = True
limit.direction = +1

def main():
    x0 = [4.62e3, np.deg2rad(-7.6), 160e3]
    t_final = 9800
    t_span = [0, t_final]
    t_eval = np.arange(0, t_final, 0.001)

    Rp = 3.3962e6
    Cd_average = 1.085
    Cl = 0.1
    Cd_0 = 0.8
    S_ref = 11
    m = 461
    u = np.deg2rad(90)

    solution = solve_ivp(DOF_Dynamics, t_span, x0, t_eval=t_eval, events=limit,
                         args=(Rp, Cd_0, Cd_average, Cl, S_ref, m, u))
    time = solution.t
    velocity = solution.y[0]
    fpa = solution.y[1]
    altitude = solution.y[2]

    # plt.plot(time, altitude)
    # plt.show()

    plt.plot(time, density(altitude))
    plt.show()
    print(min(density(altitude)))

    return velocity, altitude, fpa, time



if __name__ == "__main__":
    main()




