from Closed_form import closed_h_rho
from q_dot_compute import get_qdot, get_qdot_trajectory
import numpy as np
import matplotlib.pyplot as plt
from Heat_rate_Newton import get_angle
from Simulation import main


t_f = 650
v_0 = 4.5e3
t = np.linspace(0, t_f, num=1000)
h, Rho = closed_h_rho(160e3, v_0, -6.3, t, t_f)
heat_limit = 0.1

u_vals = []
for rho_t in Rho:

    qdot_max = get_qdot(v_0, 90, rho_t)
    print('qmax is',qdot_max)
    qdot_min = get_qdot(v_0, 0, rho_t)
    print('qmin is',qdot_min)

    if qdot_max < heat_limit:
        u = 90
        print(rho_t)
        u_vals.append(u)
        print(u)
    elif qdot_min > heat_limit:
        u = 0.00001
        print(rho_t)
        u_vals.append(u)
        print(u)
    elif qdot_max >= heat_limit >= qdot_min:
        print('Computing angle using newton', rho_t)
        u = get_angle(rho_t, v_0)
        u_vals.append(u)
        print(u)


u_profile = np.array(u_vals)

plt.figure(6)
plt.plot(t, h)
plt.show()

q_vals = get_qdot_trajectory(v_0, u_profile, Rho)





fig0, ax0 = plt.subplots(2, 1, sharex=True)
fig0.suptitle('Angle of attack, heat rate')
ax0[0].plot(t, u_profile)
ax0[1].plot(t, q_vals)
ax0[1].plot([0, t_f], [heat_limit, heat_limit], linewidth=1, linestyle='--')

ax0[0].set_ylabel('AoA, deg')
ax0[1].set_ylabel('Heat rate, W/cm^2')

plt.show()
fig0.savefig('TEST_RESULTS')
