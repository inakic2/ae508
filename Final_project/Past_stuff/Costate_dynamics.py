import numpy as np
from scipy.integrate import solve_ivp, odeint
import math
import matplotlib.pyplot as plt


def density(h):
    Rho_0 = 8.748923102971180e-07
    H = 6.308278108290950e+03
    Rho = Rho_0 * np.exp((90e3 - h) / H)

    return Rho


def gravity(h):
    g0 = 3.71
    Rp = 3.3962e6
    g = g0 * (Rp / (Rp + h)) ** 2

    return g

def DOF_Dynamics(t, x, Rp, Cd_0, Cd_average, Cl, S_ref, h_ref, H, K, m, u):
    #  inputs/x matrix
    #  v, gamma, h, lambda_v, lambda_gamma, lambda_h


    #  Two-degree-of-freedom equations of motion, states
    v_dot = - ((((density(x[2]) * S_ref * (Cd_0 + u * Cd_average)) / (2 * m)) * x[0] ** 2) + (
            gravity(x[2]) * x[1]))
    gamma_dot = ((density(x[2]) * S_ref * Cl * x[0]) / 2 / m) - (gravity(x[2]) / x[0]) + (x[0] / (Rp + x[2]))
    h_dot = x[0] * x[1]

    #  Costate equations

    lambda_v_dot = -3 * K * density(x[2]) * x[0] ** 2 * (u / np.pi) + \
                   x[3] * ((density(x[2]) * S_ref * (Cd_0 + u * Cd_average) * x[0]) / m) - \
                   x[4] * (((density(x[2]) * S_ref * Cl) / (2 * m)) + (gravity(x[2]) / x[0] ** 2) + (
            1 / (Rp + x[2]))) - \
                   x[4] * x[1]

    lambda_gamma_dot = x[3] * gravity(x[2]) - x[4] * x[0]

    lambda_h_dot = (K * density(x[2]) * x[0] ** 3 * u) / np.pi * H - \
                   x[3] ** ((density(x[2]) * S_ref * (Cd_0 + u * Cd_average) * x[0] ** 2) / (2 * m * H) + (
            (2 * gravity(x[2]) * x[1]) / (Rp + h_ref + x[2]))) + \
                   x[4] * (((density(x[2]) * S_ref * Cl * x[0]) / 2 * m * H) - (2 * gravity(x[2])) / (
            (Rp + h_ref + x[2]) * x[0]) + (x[0] / (Rp + x[2]) ** 2))

    return np.hstack([v_dot, gamma_dot, h_dot, lambda_v_dot, lambda_gamma_dot, lambda_h_dot])


def limit(t, y, Rp, Cd_0, Cd_average, Cl, S_ref, h_ref, H, K, m, u):
    return np.array(y[2] - 160000)


limit.terminal = True
limit.direction = +1


def main():
    x0 = [4.2e3, np.deg2rad(-6.9), 160e3, 4152.30, 10e-6, 3.38e-09]

    # lambda_v_f = velocity[-1]
    # lambda_gamma_f = 0
    # lambda_h_final = 42828.37362069 / (Rp + altitude[-1])**2

    t_final = 1500
    t_span = [0, t_final]
    t_eval = np.arange(0, t_final, 1)

    Rp = 3.3962e6
    Cd_average = 1
    Cl = 0.1
    Cd_0 = 0.8
    S_ref = 11
    m = 461
    u = np.deg2rad(90)
    H = 6.308278108290950e+03
    h_ref = 90e3
    K = 0.04

    solution = solve_ivp(DOF_Dynamics, t_span, x0, t_eval=t_eval, events=limit,
                         args=(Rp, Cd_0, Cd_average, Cl, S_ref, h_ref, H, K, m, u))
    time = solution.t
    velocity = solution.y[0]
    fpa = solution.y[1]
    altitude = solution.y[2]

    lambda_v = solution.y[3]
    #
    # plt.figure(1)
    # plt.plot(time, altitude)
    #
    # plt.show()
    #
    # lambda_switch = (K * velocity * 2 * m) / (np.pi * S_ref * Cd_average)
    #
    # plt.figure(2)
    # plt.plot(time, lambda_v, color="blue")
    # plt.plot(time, velocity, color='red')
    # plt.plot(time, lambda_switch, color='black')
    # plt.show()


if __name__ == "__main__":
    main()
