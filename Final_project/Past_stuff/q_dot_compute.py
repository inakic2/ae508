# from Simulation import *
#
# x0 = [4.2e3, np.deg2rad(-6.5), 160e3]
# t_final = 9800
# t_span = [0, t_final]
# t_eval = np.arange(0, t_final, 0.001)
#
# Rp = 3.3962e6
# Cd_0 = 2.6
# Cd_average = 1.018591636
# Cl = 0.1
# S_ref = 11
# m = 461
# u = np.deg2rad(90)
#
#
# def tst(AoA):
#     solution = solve_ivp(DOF_Dynamics, t_span, x0, t_eval=t_eval, events=limit,
#                          args=(Rp, Cd_0, Cd_average, Cl, S_ref, m, u))
#     time = solution.t
#     Velocity = solution.y[0]
#     fpa = solution.y[1]
#     altitude = solution.y[2]
#     Rho = density(altitude)
#
#     R = 188.92
#     T_p = 150
#
#     x = np.deg2rad(AoA)
#     q_dot = []
#     for rho, velocity in zip(Rho, Velocity):
#         S = 4.2e3 / (2 * R * T_p) ** 0.5
#         L = (0.2 * rho * R * T_p) * ((R * T_p / (2.0 * math.pi)) ** 0.5) * 1e-4
#         q_dot_vals = L * ((S ** 2 + 0.5) * (math.exp(-(S * math.sin(x)) ** 2) + (math.pi ** 0.5) * (S * math.sin(x)) *
#                                             (1 + math.erf(S * math.sin(x)))) - 0.5 * math.exp(-(S * math.sin(x)) ** 2))
#         q_dot.append(q_dot_vals)
#
#     plt.figure(2)
#     plt.title('sim')
#     plt.plot(time, q_dot)
#     plt.show()
#     print("the max is", max(q_dot))
#     print(time[-1])
#
#     return q_dot
#

import math
import numpy as np


def get_qdot_trajectory(v_0, AoA, Rho):
    R = 188.92
    T_p = 150
    u_vals = np.deg2rad(AoA)
    q_dot = []
    for rho, x in zip(Rho, u_vals):
        S = v_0 / (2 * R * T_p) ** 0.5
        L = (0.2 * rho * R * T_p) * ((R * T_p / (2.0 * math.pi)) ** 0.5) * 1e-4
        q_dot_vals = L * ((S ** 2 + 0.5) * (math.exp(-(S * math.sin(x)) ** 2) + (math.pi ** 0.5) * (S * math.sin(x)) *
                                            (1 + math.erf(S * math.sin(x)))) - 0.5 * math.exp(-(S * math.sin(x)) ** 2))
        q_dot.append(q_dot_vals)

    return q_dot


def get_qdot(v_0, AoA, rho):
    R = 188.92
    T_p = 150

    x = np.deg2rad(AoA)
    S = v_0 / (2 * R * T_p) ** 0.5
    L = (0.2 * rho * R * T_p) * ((R * T_p / (2.0 * math.pi)) ** 0.5) * 1e-4
    q_dot = L * ((S ** 2 + 0.5) * (math.exp(-(S * math.sin(x)) ** 2) + (math.pi ** 0.5) * (S * math.sin(x)) *
                                   (1 + math.erf(S * math.sin(x)))) - 0.5 * math.exp(-(S * math.sin(x)) ** 2))
    return q_dot
