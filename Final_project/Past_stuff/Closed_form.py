import numpy as np
import matplotlib.pyplot as plt
from Simulation import density
import math
# from q_dot_compute import get_qdot, tst

g_ref = 3.71
R_p = 3396e3
# v_0 = 4.2e3
# h_0 = 160e3
S_ref = 11
S_ref_sp = 7.3
S_ref_mb = 3.7
# t_f = 595
# t_p = t_f / 2
# theta = np.deg2rad(90)
AoA = 90
# gamma_0 = np.deg2rad(-6.5)
m = 426
Cd_av = 2.04
Cl_0 = 0.1
Cd_0 = 0.8

R = 188.92
T_p = 150


def closed_h_rho(h_0, v_0, gamma_0, t, t_f):
    Rho_0 = 8.748923102971180e-07
    H = 6.308278108290950e+03
    t_p = t_f / 2
    h_t = h_0 + v_0 * np.deg2rad(gamma_0) * (t - (t ** 2) / (2 * t_p))
    Rho = Rho_0 * np.exp((90e3 - h_t) / H)
    return h_t, Rho



# #  Now, k1 and k2 can be computed
#
# k = 0
# k_1 = (rho * Cl_0 * S_ref) / (2 * m) + (1 / (R_p + h_t))
# k_2 = ((rho * (Cd_0 + theta * Cd_av) * S_ref) / (2 * m)) * v_0 * gamma_0 * (1 - (t / t_p))
#
# #  Where epsilon (t), f1 and f2 are given by:
#
# f_1 = -0.005 * v_0 + 27.87
# f_2 = 1.9388e7 * np.exp(-2.426 * gamma_0 - 0.00825 * v_0) + 1.625e-8 * np.exp(-10e-6 * gamma_0 - 1.911 * v_0)
# epsilon = f_1 + f_2 * (t / (2 * t_p)) * ((theta * (S_ref_sp / S_ref)) + ((np.pi / 2) * (S_ref_mb / S_ref)))
#
# #  Now, the velocity and FPA can be computed
#
# v_t = k_2 / (2 * k_1) - 0.5 * (((k_2 / k_1) ** 2 + 4 * ((g_ref + epsilon) / k_1)) ** 0.5) + k
# gamma_t = ((v_0 * gamma_0) / v_t) * (1 - t / t_p)


#
# #  Auxiliary coefficients
#

#
#
# #  equations definition
#
