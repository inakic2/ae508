import math
import numpy as np
from planet import Planet, mars_props
from vehicle import Vehicle, heat_rate_props
from Control_logic import Trajectory
import matplotlib.pyplot as plt
from AoA_profile import get_aoa_profile
from utils import get_qdot_trajectory


heat_rate = Vehicle(properties=heat_rate_props)
mars = Planet(properties=mars_props)

solutions = Trajectory(vehicle=heat_rate, planet=mars)
v, h, gamma, t, aoa_profile = solutions.closed_form()

aoa_profile_array = get_aoa_profile(v, h, gamma)
aoa_profile2 = aoa_profile_array.tolist()


q_dot = get_qdot_trajectory(v, aoa_profile_array, mars.rho(h))
Q = np.cumsum(q_dot) * t[-1] / len(t)
print(Q[-1])

fig0, ax0 = plt.subplots(3, 1, sharex=True)
fig0.suptitle('Heat  control Solution')
ax0[0].plot(t, aoa_profile_array)
ax0[1].plot(t, q_dot)
ax0[1].plot([0, t[-1]], [0.16, 0.16], linewidth=1, linestyle='--')
ax0[2].plot(t, Q)

ax0[0].set_ylabel('AoA, deg')
ax0[1].set_ylabel('Heat rate, W/cm^2')
ax0[2].set_ylabel('Heat load, J/cm^2')
ax0[2].set_xlabel('Time, s')

ax0[2].set_ylim(-5, 50)

plt.show()
fig0.savefig('Solution_Heat_rate')

