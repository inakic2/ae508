import numpy as np


class Vehicle:
    def __init__(self, properties):
        if isinstance(properties, dict):
            self.m = properties['m'] if 'm' in properties else None
            self.Cl_0 = properties['Cl_0'] if 'Cl_0' in properties else None
            self.Cd_0 = properties['Cd_0'] if 'Cd_0' in properties else None
            self.Cd_slope = properties['Cd_slope'] if 'Cd_slope' in properties else None
            self.v0 = properties['v0'] if 'v0' in properties else None
            self.h0 = properties['h0'] if 'h0' in properties else None
            self.gamma0 = properties['gamma0'] if 'gamma0' in properties else None
            self.S_ref_total = properties['S_ref_total'] if 'S_ref_total' in properties else None
            self.S_ref_SA = properties['S_ref_SA'] if 'S_ref_SA' in properties else None
            self.S_ref_SC = properties['S_ref_SC'] if 'S_ref_SC' in properties else None
            self.aoa = properties['aoa'] if 'aoa' in properties else None
        elif isinstance(properties, Vehicle):
            self.m = properties.m
            self.Cl_0 = properties.Cl_0
            self.Cd_0 = properties.Cd_0
            self.Cd_slope = properties.Cd_slope
            self.v0 = properties.v0
            self.h0 = properties.h0
            self.gamma0 = properties.gamma0
            self.S_ref_total = properties.S_ref_total
            self.S_ref_SA = properties.S_ref_SA
            self.S_ref_SC = properties.S_ref_SC
            self.aoa = properties.aoa


heat_rate_props = {
    'm': 461,
    'Cl_0': 0.1,
    'Cd_0': 0.8,
    'Cd_slope': 1.085,
    'v0': 4.5e3,
    'h0': 160e3,
    'gamma0': np.deg2rad(-7.6),
    'S_ref_total': 11,
    'S_ref_SA': 3.74,
    'S_ref_SC': 7.26,
    'aoa': np.deg2rad(90)
}
