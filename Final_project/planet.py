import numpy as np


class Planet:
    def __init__(self, properties):
        self.name = properties['name'] if 'name' in properties else None
        self.Rp = properties['Rp'] if 'Rp' in properties else None
        self.H = properties['H'] if 'H' in properties else None
        self.rho_ref = properties['rho_ref'] if 'rho_ref' in properties else None
        self.g_ref = properties['g_ref'] if 'g_ref' in properties else None
        self.mu = properties['mu'] if 'mu' in properties else None
        self.h_ref = properties['h_ref'] if 'h_ref' in properties else None

    def rho(self, h):
        # return self.rho_ref * np.exp(1 / self.H * (self.h_ref - h))
        return self.rho_ref * np.exp(np.divide(self.h_ref - h, self.H))

    def grav(self, h):
        return self.g_ref * (self.Rp / (self.Rp + h)) ** 2


mars_props = {
    'name': 'Mars',  # Planet name
    'Rp': 3.3962e6,  # Equatorial radius[m]
    'H': 6.3e+03,  # Reference scale height [m]
    'rho_ref': 8.748923102971180e-07,  # Reference density [kg/m^3]
    'g_ref': 3.71,  # Surface gravity [m/s^2]
    'm': 4.2828e13 / 6.6743e-11,  # Planet mass [kg]
    'omega': 7.0882e-5,  # Planet rotation rate [rad/s]
    'mu': 4.2828e13,  # Standard gravitational parameter [m^3/s^2]
    'h_ref': 90e3,  # Reference altitude for exponential atmosphere [m]
}
