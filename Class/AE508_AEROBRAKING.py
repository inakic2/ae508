# problem 4, homework 3

# importing libraries

import numpy as np
from scipy.integrate import solve_ivp, odeint
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d


##defining the method to solve

# defining the density profile
def density_profile(h):
    Rho_0 = 0.02
    H = 11000
    Rho = Rho_0 * np.exp(-h / H)
    return Rho


# Inverse square gravity, varying density

def varying_gravity_density(t, x, Rp, Beta, L_D, g0):
    # setting the values to the array
    v = x[0]
    gamma = x[1]
    h = x[2]
    u = 45
    Cd_0 = 0.6
    Cl = 0
    S_ref = 7.26
    Cd_var = 0
    m = 461

    # Radius of earth, density
    R_p = 3396e4
    rho = density_profile(h)

    # Varying gravity model
    g = g0 * (Rp / (Rp + h)) ** 2

    # equations of motion 2DOF

    v_dot = - (((rho * S_ref * (Cd_0 + u * Cd_var)) / (2 * m)) * v ** 2) - (g * gamma)
    gamma_dot = ((rho * S_ref * Cl * v) / 2 * m) - (g / v) + (v / (R_p + h))
    h_dot = v * gamma
    # dvdt = -((rho) * ((v) ** 2)) / (2 * Beta) - g * np.sin(gamma)
    # dgammadt = (((rho * v) / (2 * Beta)) * L_D) - ((g / v) * np.cos(gamma)) + ((v * np.cos(gamma)) / (Rp + h))
    # dhdt = v * np.sin(gamma)
    # dthetadt = (((v) * np.cos(gamma)) / ((Rp) + (h)))
    dxdt = [v_dot, gamma_dot, h_dot]

    return dxdt


def limit(t, y, Rp, Beta, L_D, g0):
    return np.array(y[2] - 125000)


limit.terminal = True
limit.direction = +1

# creating the initial

v0 = 15 * 1000  # in m
gamma0 = -2 * np.pi / 180
h0 = 160000  # in m
L_D = 0
L_D_normal = L_D
Beta = 50
Rp = 3396e4
g0 = 3.71
rn = 0.24

x0 = [v0, gamma0, h0]
t_final = 1000

t_span = [0, t_final]
t_eval = np.arange(0, t_final, 0.001)
Solution = solve_ivp(varying_gravity_density, t_span, x0, events=limit, t_eval=t_eval, args=(Rp, Beta, L_D_normal, g0))

time = Solution.t
vel = Solution.y[0]
height_sol = Solution.y[2]
gamma = Solution.y[1] * 180 / np.pi


# results in W/cm2


def q_conv(rn, vel):
    k = 1.7623 * 10 ** (-4)  # kg^0.5/m
    h = height_sol
    rho = density_profile(h)  # calling function to get density(h)
    index = 0
    q_convective = (k * np.sqrt(rho / rn) * vel ** 3) * 10 ** -4  # W/cm^2
    return q_convective


q_convective = q_conv(vel, rn)


def heatrate_radiative(rn, vel):
    h = height_sol
    rho = density_profile(h)  # calling function to get density(h)
    C = 4.736 * 10 ** 4
    b = 1.22
    fv = [2040, 1780, 1550, 1313, 1065, 850, 660, 495, 359, 238, 151, 115, 81, 55, 35, 19.5, 9.7, 4.3, 1.5, 0]
    vf = [16000, 15500, 15000, 14500, 14000, 13500, 13000, 12500, 12000, 11500, 11000, 10750, 10500, 10250, 10000, 9750,
          9500, 9250, 9000, 0]  # m/s
    fun = interp1d(vf, fv)
    a = 1.072 * 10 ** 6 * vel ** (-1.88) * rho ** (-0.325)
    f = fun(vel)
    q_rad = (C * (rn ** a) * (rho ** b) * f)  # W/cm^2
    return q_rad


q_radiative = heatrate_radiative(rn, vel)

##To find the surface temperature:
fig = plt.figure(figsize=(8, 6), dpi=100, facecolor='w', edgecolor='k')

# sigma = 5.67e-8
# epsilon = 0.95
# T_surface_4 = ((q_total * 10000) / (sigma * epsilon))
# T_surface = T_surface_4 ** ((0.25))
# plt.figure()
# fig = plt.figure(figsize=(8, 6), dpi=100, facecolor='w', edgecolor='k')
# plt.plot(T_surface, time)
# plt.grid()
# plt.ylabel("Time, s")
# plt.xlabel("Wall Temperature, K")
# print(max(T_surface))
#
# plt.figure(1)
# fig = plt.figure(figsize=(8, 6), dpi=100, facecolor='w', edgecolor='k')
# plt.plot(time, q_convective, label='convective heating')
# plt.plot(time, q_radiative)
# # plt.xlim(0, 100)
# # plt.ylim(-0.5, (/max(q_convective) + 2))
# plt.xlabel("Time of flight, [s]")
# plt.ylabel("Heat rate, [W/cm$^2$]")
# plt.legend(loc="center right")
# plt.title("Heat rate as a function of time")
# dot = '\u0307'
# plt.grid()
#
# x = time
# y = q_radiative
#
#
# def annot_max(x, y, ax=None):
#     xmax = x[np.argmax(y)]
#     ymax = y.max()
#     text = "$\dotq$$_r$ ={:.3f} W/cm$^2$".format(ymax)
#     if not ax:
#         ax = plt.gca()
#     bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
#     arrowprops = dict(arrowstyle="->", connectionstyle="angle,angleA=0,angleB=60")
#     kw = dict(xycoords='data', textcoords="axes fraction",
#               arrowprops=arrowprops, bbox=bbox_props, ha="right", va="top")
#     ax.annotate(text, xy=(xmax, ymax), xytext=(0.9, 0.85), **kw)
#
#
# annot_max(x, y)
#
# plt.show()
# #
# # Heat_load_radiative = np.trapz(q_radiative, time)
# # Heat_load_convective = np.trapz(q_convective, time)
# # Total_heat_load = np.trapz(q_total, time)
#
#
# plt.figure(4)
# plt.plot(vel, height_sol)
# # plt.plot([14600, 15250], [110000, 110000], 'k-', lw=2)
#
# # plt.xlim(12900, 13000)
# # plt.ylim(50000, h0)
# plt.show()
#
# print(vel[-1])

plt.figure(5)
plt.plot(time, height_sol)
plt.show()
plt.figure(6)
plt.plot(time, gamma)
plt.show()