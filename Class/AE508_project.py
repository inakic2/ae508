# Imports
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import least_squares, root, fsolve
from scipy.integrate import solve_ivp
from scipy import integrate
import math


# %%
# Dynamics
def ode_function(t, z):
    # Unpack input


    # state differential equations
    # Have to include the gravity and atmosphere models too
    v_dot = - ((rho * S_ref * (Cd_0 + u * Cd_var)) / (2 * m)) * v ** 2 - (g * gamma)
    gamma_dot = ((rho * S_ref * Cl * v) / 2 * m) - (g / v) + (v / (R_p + h))
    h_dot = v * gamma

    # Costate differential equations
    lambda_v_dot = -3 * K * rho * v ** 2 * (u / np.pi) + lambda_v * (
    (rho * S_ref * (Cd_0 + u * Cd_var) * v)/m) - lambda_gamma * (
                               ((rho * S_ref * Cl_0) / 2 * m) + (g / v ** 2) + (1 / (R_p + h))) - lambda_h*gamma

    lambda_gamma_dot = lambda_v*g - lambda_h*v

    lambda_h_dot = (K*rho*v**3*u)/np.pi*H - lambda_v**((rho * S_ref * (Cd_0 + u * Cd_var) *
                                                       v**2)/(2*m*H) + ((2*g*gamma)/(R_p + h_ref + h)))
                                                      + lambda_gamma*(((rho * S_ref * Cl_0 * v) / 2 * m * H) - \
                                                      (2*g)/(R_p + h_ref + h)*v + (v/(R_p + h)**2))


    return np.hstack([x_dot, lam_dot])


# %%
# Cost function
def cost_function(lam0, tspan, x0, xf, rtol=3e-14, atol=3e-14, debug=False):
    # Integrate dynamics
    z0 = np.hstack([x0, lam0])
    traj = solve_ivp(ode_function, tspan, z0, method='DOP853', rtol=rtol, atol=atol)
    # Compute cost

    cost = traj.y[0:9, -1] - xf
    cost_nan = np.isnan(cost)
    cost_list = ~ cost_nan
    cost_array = cost[cost_list]
    # Print function val
    if debug:
        print("Error norm: {}".format(np.linalg.norm(cost_array)))

    # Return output
    return cost_array


# %%
# Hamiltonian
def hamiltonian_function(t, z):
    # Preallocate output
    H = np.zeros(z.shape[1])

    # Compute Hamiltonian
    for i in range(z.shape[1]):
        dz = ode_function(t[i], z[:, i])
        H[i] = np.dot(z[2:4, i], dz[0:2]) + 0.5 * z[3, i] ** 2

    # Return output
    return H


def main():
    # %%
    # Boundary conditions
    # r, theta, u , v , lambda_r, lambda_theta, lambda_u, lambda_v

    DU = 1.496e8  # DU = 1 AU
    TU = 58.13
    mu = 1  # Normalized Gravitational Parameter

    # Setting tspan
    tspan = (0, 150 / TU)

    # Building the boundary arrays
    x0, xf = np.array([1, 0, 0, 1]), np.array([1.524, np.nan, 0, np.sqrt(mu / 1.524), np.nan, 0, np.nan, np.nan])
    lam0 = np.array([0.01, 0.01, 0.01, 0.01])  # lambda_r, lambda_theta, lambda_u, lambda_v

    # Solve optimal control problem
    lam0 = root(cost_function, lam0, args=(tspan, x0, xf), method='lm', options={'xtol': 1e-14, 'ftol': 1e-14}).x

    # # Integrate converged solution
    z0 = np.hstack([x0, lam0])
    traj = solve_ivp(ode_function, tspan, z0, method='LSODA', rtol=3e-14, atol=3e-14)

    # Setting up cost calculation
    L = 0.5 * ((-traj.y[6]) ** 2 + (-traj.y[7]) ** 2)
    L_cumulative = np.trapz(L, traj.t)
    time = traj.t

    plt.figure()
    f = integrate.cumtrapz(L, time)
    plt.plot(time[1:], f)
    plt.title('Running cumulative cost')
    plt.xlabel('time')
    plt.ylabel('cost, DU^2/TU^3')
    plt.show()

    # Plot states
    fig0, ax0 = plt.subplots(4, 1, sharex=True)
    fig0.suptitle('States')
    ax0[0].plot(traj.t, traj.y[0, :])
    ax0[1].plot(traj.t, traj.y[1, :])
    ax0[2].plot(traj.t, traj.y[2, :])
    ax0[3].plot(traj.t, traj.y[3, :])
    ax0[0].set_ylabel('r')
    ax0[1].set_ylabel('theta')
    ax0[2].set_ylabel('u')
    ax0[3].set_ylabel('v')
    ax0[3].set_xlabel('t')
    plt.show()
    fig0.savefig('States')

    # Plot costates
    fig1, ax1 = plt.subplots(4, 1, sharex=True)
    fig1.suptitle('Costates')
    ax1[0].plot(traj.t, traj.y[4, :])
    ax1[1].plot(traj.t, traj.y[5, :])
    ax1[2].plot(traj.t, traj.y[6, :])
    ax1[3].plot(traj.t, traj.y[7, :])
    ax1[0].set_ylabel('lambda_r')
    ax1[1].set_ylabel('lambda_theta')
    ax1[2].set_ylabel('lambda_u')
    ax1[3].set_ylabel('lambda_v')
    ax1[3].set_xlabel('t')
    plt.show()
    fig1.savefig('costates')

    fig3 = plt.figure('r vs theta')
    plt.polar(traj.y[1], traj.y[0])
    plt.title("r vs theta")
    plt.show()
    fig3.savefig('rvstheta')

    fig4, ax2 = plt.subplots(2, 1, sharex=True)
    fig4.suptitle('Control')
    ax2[0].plot(traj.t, -traj.y[6, :])
    ax2[1].plot(traj.t, -traj.y[7, :])
    ax2[0].set_ylabel('u_r')
    ax2[1].set_ylabel('u_theta')
    plt.show()


if __name__ == "__main__":
    main()
