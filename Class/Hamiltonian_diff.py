import sympy as sp
import numpy as np
from sympy import symbols, diff
from sympy.abc import mu

u_r, u_theta, lambda_r, lambda_theta, lambda_u, lambda_v, r, u, v, theta, T, m, u1, u2, delta = symbols(
    'u_r u_theta lambda_r lambda_theta '
    'lambda_u lambda_v r u v theta T m u1 u2 delta',
    real=True)

# # Defining performance index
#
# J = (0.5 * (u_r ** 2 + u_theta ** 2)
#
# # Lambda definition
#
# Lambda = np.array([[lambda_r], [lambda_theta], [lambda_u], [lambda_v]])
#
# # X-dot definition
#
# X_dot = np.array([ [u], [(v / r)], [((v ** 2 / r) - (mu / r ** 2) + u_r)], [(-u * v / r) + u)] ])
int
Ham = (T / m) * delta + (u * lambda_r) + ((v / r) * lambda_theta) + ((((v ** 2 / r) - (mu / r ** 2) + (T / m) * delta
                                                                         * u1) * lambda_u) + (((-u * v / r) + (T / m) * delta * u2) * lambda_v))

Ham_fun_delta_u = (diff(-Ham, u))
Ham_fun_delta_v = (diff(-Ham, v))
Ham_fun_delta_r = (diff(-Ham, r))
Ham_fun_delta_theta = (diff(-Ham, theta))

Lambda_costates = np.array([[Ham_fun_delta_r], [Ham_fun_delta_theta], [Ham_fun_delta_u], [Ham_fun_delta_v]])
print(Lambda_costates)

X_dot = np.array([[((v ** 2 / r) - (mu / r ** 2) - lambda_u)], [(-u * v / r) - lambda_v], [u], [(v / r)]])
print(X_dot)
