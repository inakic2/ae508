# AE598 HW4

# Imports
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import least_squares, root, fsolve
from scipy.integrate import solve_ivp
from scipy import integrate
import math


# Dynamics definition

def ode_function(t, z):
    # Unpacking the inputs
    mu = 1
    T = 1.9921e-29  # Have to convert to AU/TU2
    c = 4155 * 9.81
    r, theta, u, v, m = z[0:5]
    lambda_r, lambda_theta, lambda_u, lambda_v = z[5:9]
    rho = 0.01

    # Switching function
    p = -np.array([lambda_u, lambda_v])
    S = np.linalg.norm(p) - 1

    # Delta
    delta = 0.5 * (1 + np.tan(S / rho))  # rho is continuation parameter [1 through 1e-3]
    print(delta)

    # State derivatives
    x_dot = np.array([u,
                      v / r,
                      (v ** 2 / r) - (mu / r ** 2) + (T / m) * delta * (-lambda_u / (np.linalg.norm(p))),
                      (-u * v) / r + (T / m) * delta * (-lambda_v / np.linalg.norm(p)),
                      (-T / c) * delta])

    # Costate derivatives
    lam_dot = np.array([((lambda_theta * v) / r ** 2) - lambda_u * (((2 * mu) / r ** 3)
                                                                    - (v ** 2) / r ** 2) - (lambda_v * u * v) / r ** 2,
                        0,
                        -lambda_r + (lambda_v * v / r),
                        (-lambda_theta / r) - (2 * lambda_u * v / r) + (lambda_v * u) / r])

    return np.hstack([x_dot, lam_dot])


# Cost function
def cost_function(lam0, tspan, x0, xf, rtol=3e-14, atol=3e-14, debug=True):
    # Integrate dynamics
    z0 = np.hstack([x0, lam0])
    traj = solve_ivp(ode_function, tspan, z0, method='DOP853', rtol=rtol, atol=atol)
    # Compute cost
    print(traj.y[0:10, -1])
    cost = traj.y[0:10, -1] - xf
    cost_nan = np.isnan(cost)
    cost_list = ~ cost_nan
    cost_array = cost[cost_list]
    # Print function val
    if debug:
        print("Error norm: {}".format(np.linalg.norm(cost_array)))

    # Return output
    return cost_array


def main():
    #  Boundary conditions: r, theta, u , v

    DU = 1.496e8  # DU = 1 AU
    TU = 58.13
    mu = 1  # Normalized Gravitational Parameter
    rho = 1

    # Setting tspan
    tspan = (0, 300 / TU)

    # Building the boundary arrays
    x0, xf = np.array([1, 0, 0, 1, 500/500]), np.array(
        [1.524, np.nan, 0, np.sqrt(mu / 1.524), np.nan, np.nan, 0, np.nan, np.nan])
    lam0 = np.array([-2, 0, -0.5, -2])  # lambda_r, lambda_theta, lambda_u, lambda_v

    # Solve optimal control problem
    lam0 = root(cost_function, lam0, args=(tspan, x0, xf), method='lm', options={'xtol': 1e-14, 'ftol': 1e-14}).x

    # # Integrate converged solution
    z0 = np.hstack([x0, lam0])
    traj = solve_ivp(ode_function, tspan, z0, method='LSODA', rtol=3e-14, atol=3e-14)
    print(traj.y[0])


if __name__ == "__main__":
    main()
