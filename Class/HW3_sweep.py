# Imports
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import least_squares, root, fsolve
from scipy.integrate import solve_ivp
import math


# %%
# Dynamics
def ode_function(t, z):
    # Unpack input
    mu = 1
    r, theta, u, v = z[0:4]
    lambda_r, lambda_theta, lambda_u, lambda_v = z[4:8]

    # State derivative
    x_dot = np.array([u, v / r, -lambda_u - mu / r ** 2 + v ** 2 / r, -lambda_v - u * v / r])

    # Costate derivative
    lam_dot = np.array([((lambda_theta * v) / r ** 2) - lambda_u * (((2 * mu) / r ** 3) - (v ** 2) / r ** 2) -
                        (lambda_v * u * v) / r ** 2, 0, -lambda_r + (lambda_v * v / r), (-lambda_theta / r) -
                        (2 * lambda_u * v / r) + (lambda_v * u) / r])

    return np.hstack([x_dot, lam_dot])


# %%
# Cost function
def cost_function(lam0, tspan, x0, xf, rtol=3e-14, atol=3e-14, debug=True):
    # Integrate dynamics
    z0 = np.hstack([x0, lam0])
    traj = solve_ivp(ode_function, tspan, z0, method='DOP853', rtol=rtol, atol=atol)
    # Compute cost

    cost = traj.y[0:9, -1] - xf
    cost_nan = np.isnan(cost)
    cost_list = ~ cost_nan
    cost_array = cost[cost_list]
    # Print function val
    if debug:
        print("Error norm: {}".format(np.linalg.norm(cost_array)))

    # Return output
    return cost_array


# %%
# Hamiltonian
def hamiltonian_function(t, z):
    # Preallocate output
    H = np.zeros(z.shape[1])

    # Compute Hamiltonian
    for i in range(z.shape[1]):
        dz = ode_function(t[i], z[:, i])
        H[i] = np.dot(z[2:4, i], dz[0:2]) + 0.5 * z[3, i] ** 2

    # Return output
    return H


def main():
    # %%
    # Boundary conditions
    # r, theta, u , v , lambda_r, lambda_theta, lambda_u, lambda_v

    times = np.array([70, 150, 200, 360, 600])
    vals_L = []
    for val in times:

        DU = 1.496e8  # DU = 1 AU
        TU = 58.13
        mu = 1  # Normalized Gravitational Parameter
        # Setting tspan
        tspan = (0, val / TU)

        # Building the boundary arrays
        x0, xf = np.array([1, 0, 0, 1]), np.array([1.524, np.nan, 0, np.sqrt(mu / 1.524), np.nan, 0, np.nan, np.nan])
        lam0 = np.array([0.01, 0.0000000001, 0.01, 0.01])  # lambda_r, lambda_theta, lambda_u, lambda_v

        # Solve optimal control problem
        lam0 = root(cost_function, lam0, args=(tspan, x0, xf), method='lm', options={'xtol': 1e-10, 'ftol': 1e-12}).x

        # # Integrate converged solution
        z0 = np.hstack([x0, lam0])
        traj = solve_ivp(ode_function, tspan, z0, method='LSODA', rtol=10e-10, atol=10e-12)

        # Setting up cost calculation
        L = np.array(0.5 * ((-traj.y[6]) ** 2 + (-traj.y[7]) ** 2))
        L_cumulative = np.trapz(L, traj.t)
        vals_L.append(L_cumulative)
        print(vals_L)

    plt.plot(times/58.13, vals_L)
    plt.ylabel('Cummulative cost, DU^2/TU^3')
    plt.xlabel('TOF, TU')
    plt.title("TOF v cost parameter sweep")
    plt.show()

        # # Plot states
        # fig0, ax0 = plt.subplots(4, 1, sharex=True)
        # fig0.suptitle('States')
        # ax0[0].plot(traj.t, traj.y[0, :])
        # ax0[1].plot(traj.t, traj.y[1, :])
        # ax0[2].plot(traj.t, traj.y[2, :])
        # ax0[3].plot(traj.t, traj.y[3, :])
        # ax0[0].set_ylabel('r')
        # ax0[1].set_ylabel('theta')
        # ax0[2].set_ylabel('u')
        # ax0[3].set_ylabel('v')
        # ax0[3].set_xlabel('t')
        # plt.show()
        # fig0.savefig('States')
        #
        # # Plot costates
        # fig1, ax1 = plt.subplots(4, 1, sharex=True)
        # fig1.suptitle('Costates')
        # ax1[0].plot(traj.t, traj.y[4, :])
        # ax1[1].plot(traj.t, traj.y[5, :])
        # ax1[2].plot(traj.t, traj.y[6, :])
        # ax1[3].plot(traj.t, traj.y[7, :])
        # ax1[0].set_ylabel('lambda_r')
        # ax1[1].set_ylabel('lambda_theta')
        # ax1[2].set_ylabel('lambda_u')
        # ax1[3].set_ylabel('lambda_v')
        # ax1[3].set_xlabel('t')
        # plt.show()
        # fig1.savefig('costates')
        #
        # fig3 = plt.figure('r vs theta')
        # plt.polar(traj.y[1], traj.y[0])
        # plt.title("r vs theta")
        # plt.show()
        # fig3.savefig('rvstheta')
        #
        # fig4,ax2 = plt.subplots(2,1, sharex=True)
        # fig4.suptitle('Control')
        # ax2[0].plot(traj.t, -traj.y[6, :])
        # ax2[1].plot(traj.t, -traj.y[7, :])
        # ax2[0].set_ylabel('u_r')
        # ax2[1].set_ylabel('u_theta')
        # plt.show()

if __name__ == "__main__":
    main()
